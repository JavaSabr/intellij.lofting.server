package com.cigital.intelj.lofting;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by ronn on 09.07.15.
 */
@RestController
public class ReceiveFileController {

    private static final String LOFTING_FOLDER_NAME = ".lofting";
    private static final String USER_HOME = "user.home";

    private Path workFolderPath;

    private Path getWorkFolderPath() {

        if (workFolderPath == null) {
            workFolderPath = Paths.get(System.getProperty(USER_HOME), LOFTING_FOLDER_NAME);
        }

        return workFolderPath;
    }

    @RequestMapping(value = "/upload_file", method = RequestMethod.POST)
    public
    @ResponseBody
    String uploadFile(@RequestParam("file") final MultipartFile file, @RequestParam("path") final String path) {

        if (file.isEmpty()) {
            System.out.println("File " + path + " is empty.");
            return "File " + path + " is empty.";
        }

        final Path workFolder = getWorkFolderPath();

        if (!Files.exists(workFolder)) {
            try {
                Files.createDirectories(workFolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final Path resolveFile = workFolder.resolve(path);

        if (Files.isDirectory(resolveFile) && !Files.exists(resolveFile)) {
            try {
                Files.createDirectories(resolveFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            final Path parentFolder = resolveFile.getParent();
            if (!Files.exists(parentFolder)) {
                try {
                    Files.createDirectories(parentFolder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            Files.deleteIfExists(resolveFile);
            Files.copy(file.getInputStream(), resolveFile);
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }

        return "File " + resolveFile.getFileName() + " uploaded.";
    }
}
