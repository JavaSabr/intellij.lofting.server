package com.cigital.intelj.lofting;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ronn on 09.07.15.
 */
@RestController
public class GetFileController {

    @RequestMapping(value = "/get_content", method = RequestMethod.GET)
    public
    @ResponseBody
    String getContent() {
        return "<html>\n" +
                "<head>\n" +
                "<script>\n" +
                "var rnd = Math.random;\n" +
                "\n" +
                "setInterval(function () {\n" +
                "\n" +
                "\tvar c = document.getElementById(\"myCanvas\");\n" +
                "\tvar ctx = c.getContext(\"2d\");\n" +
                "\tctx.fillStyle = \"rgb(\" + rgb() + \",\" + rgb() + \",\" + rgb() + \")\";\n" +
                "\tctx.fillRect(c.width * rnd(), c.height * rnd(), rnd() * c.height/4, rnd() * c.width/4);\n" +
                "}, 100);\n" +
                "\n" +
                "function rgb() {\n" +
                "   return Math.floor(rnd()*256).toString();\n" +
                "}\n" +
                "</script>\n" +
                "</head>\n" +
                "<body>\n" +
                "<canvas id=\"myCanvas\" width=\"500\" height=\"200\" style=\"border:1px solid #000000;\">\n" +
                "Your browser does not support the HTML5 canvas tag.\n" +
                "</canvas>\n" +
                "</body>\n" +
                "</html>";
    }
}
